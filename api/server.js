'use strict'
require('dotenv').config({silent:true})
const express = require('express')
const bodyParser = require ( 'body-parser')
const Sequelize = require('sequelize') //biblioteci
const Op = Sequelize.Op
const UserRouter = require('./routers/user-router')
const ProjectRouter = require('./routers/project-router')
const BugRouter = require('./routers/bug-router')
const TeamTesterRouter = require('./routers/teamTester-router')
const TeamMemberRouter = require('./routers/teamMember-router')
const cors = require('cors') 



const app = express()
app.use(cors())
app.use(bodyParser.json())
app.use(express.static('static'))
app.use('/user-api',UserRouter)
app.use('/project-api', ProjectRouter)
app.use('/bug-api', BugRouter)
app.use('/teamTester-api', TeamTesterRouter)
app.use('/teamMember-api', TeamMemberRouter)





app.get('/bugs',async (req,res)=>{
	let bug =await Bug.findAll()
	res.status(200).json(bug)
})//tested




app.post('/bugs/:id',async(req,res)=>{
		try{
			if(req.query.bulk&&req.query.bulk =='on'){
				await Bug.bulkCreate(req.body)
				res.status(201).json({message : "bulk inserted succesfully"})
			}else{

				await Bug.create({
					description : req.body.description , 
					severity : req.body.severity,
					priority : req.body.priority,
					link : req.body.link,
					projectId: req.params.id
				})
				res.status(201).json({message : " inserted succesfully"})
			}
				
		}catch(err){
			console.warn(err)
			res.status(500).json({message:'internal server error'})
		}

})//tested











app.delete('/bugs/:id',async(req,res)=>{try{
		let user = await Bug.findByPk(req.params.id)
		if(user){
			await user.destroy()
			res.status(202).json({message : "accepted"})
		}else{
			res.status(404).json({message : "not found"})
		}

	}catch(err){
		console.warn(err)
		res.status(404).json({message : "internal server error"})
	}


})//tested




app.put('/bugs/:id',async(req,res)=>{try{
		let user = await Bug.findByPk(req.params.id)
		if(user){
			await user.update(req.body)
			res.status(202).json({message : "accepted"})
		}else{
			res.status(404).json({message : "not found"})
		}

	}catch(err){
		console.warn(err)
		res.status(404).json({message : "internal server error"})
	}


})//tested















//to do  add a bug to a project ; //done /bugs/projectId  : make verification for project id
//alter the bug from member perspective;
//assign bug to mmber



app.listen(8080)
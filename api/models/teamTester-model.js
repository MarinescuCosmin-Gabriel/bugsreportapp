module.exports= (sequelize ,DataTypes)=>{
	return sequelize.define('team_tester', {
		projectId : {
		type : DataTypes.INTEGER,
		allowNull: false,
		defaultValue:0
	},
	userId : {
		type : DataTypes.INTEGER,
		allowNull: false,
		defaultValue:0
	}
})
}
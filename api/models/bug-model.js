module.exports= (sequelize ,DataTypes)=>{
	return sequelize.define('bug', {
	description : {
		type : DataTypes.TEXT,
		allowNull: false
	},
	severity : {
		type : DataTypes.INTEGER,
		allowNull : false,
		validate : {
			isInt : true,
			isCif ( value ){
				if(!(parseInt(value)>=1&&parseInt(value)<=10)){
					throw new Error('Severity must be in interval 1 to 10')
				}
			}
		}
	},
	priority : {
		type : DataTypes.INTEGER,
		allowNull : false,
		validate : {
			isInt : true,
			isCif ( value ){
				if(!(parseInt(value)>=1&&parseInt(value)<=10)){
					throw new Error('Priority must be in interval 1 to 10')
				}
			}
		}
	},

	link : {
		type : DataTypes.TEXT , 
		allowNull : false , 
		defaultValue : "no link to commit"
	},
	projectId : {
		type : DataTypes.INTEGER,
		allowNull: false,
		defaultValue:0
	},
	assignedTo : {
		type : DataTypes.STRING,
		allowNull: true,
		defaultValue: null
	},
	reportedBy : {
		type : DataTypes.INTEGER,
		allowNull:false,
		
	},
	status : {
		type : DataTypes.STRING,
		defaultValue : "unresolved"
	}



})
}
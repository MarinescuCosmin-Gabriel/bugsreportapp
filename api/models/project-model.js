module.exports= (sequelize ,DataTypes)=>{
	return sequelize.define('project',{
	name : {
		type: DataTypes.STRING,
		allowNull : false,
		
	},
	description : {
		type : DataTypes.TEXT,
		allowNull : false,
	},
	
	 repo : {
	 	type : DataTypes.TEXT,
	 	allowNull: false
	 }
})
}
module.exports= (sequelize ,DataTypes)=>{
	return sequelize.define('user',{
	email : {
		type:DataTypes.STRING,
		allowNull:false,
		validate : {
			isEmail : true
		}
	},

	password : {
		type:DataTypes.STRING,
		allowNull:false,
		validate : {
			isAlphanumeric: true
		}
	}
	
})
}
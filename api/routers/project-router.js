'use strict '
const express= require('express')
const router = express.Router()
const Sequelize = require('sequelize')
const cors = require('cors')

const sequelize = new Sequelize(process.env.DB,process.env.DB_USER,process.env.DB_PASSWORD,{
	dialect : 'mysql',

})

const Project = sequelize.import('../models/project-model')
const TeamTester = sequelize.import('../models/teamTester-model')
const TeamMember = sequelize.import('../models/teamMember-model')
const Bug = sequelize.import('../models/Bug-model')
router.use(cors())

router.get('/projects',async (req,res)=>{
	let project =await Project.findAll()
	res.status(200).json(project)
})//get all projects

router.get('/projects/:id',async (req,res)=>{

	let project = await Project.findByPk(req.params.id) //nu scrie constanta la fel ca tabela de mai sus !! 
	if(project){
		

		res.status(200).json(project)
	}else{
		res.status(404).json({message: 'not found'})
	}


})

router.post('/projects',async(req,res)=>{
		try{
			if(req.query.bulk&&req.query.bulk =='on'){
				await Project.bulkCreate(req.body)
				res.status(201).json({message : "bulk inserted succesfully"})
			}else{

				 let proj = await Project.create(req.body)
				res.status(201).json(proj)
			}
				
		}catch(err){
			console.warn(err)
			res.status(500).json({message:'internal server error'})
		}

})//insert project

router.delete('/projects/:id',async(req,res)=>{try{
		let project = await Project.findByPk(req.params.id)
		if(project){
			
		let teamTs = await TeamTester.findAll({where : {projectId : project.id}})
		if (teamTs) {

			await teamTs.forEach((e,i)=>{e.destroy()})
		}
		let teamMs = await TeamMember.findAll({where : {projectId : project.id}})
		if (teamMs) {
			await teamMs.forEach((e,i)=>{e.destroy()})
		}
			await project.destroy()
			res.status(202).json({message : "accepted"})
		}else{
			res.status(404).json({message : "not found"})
		}

	}catch(err){
		console.warn(err)
		res.status(404).json({message : "internal server error"})
	}
})//tested

router.put('/projects/:id',async(req,res)=>{
	try{
		let user = await Project.findByPk(req.params.id)
		if(user){
			await user.update(req.body)
			res.status(202).json({message : "accepted"})
		}else{
			res.status(404).json({message : "not found"})
		}

	}catch(err){
		console.warn(err)
		res.status(404).json({message : "internal server error"})
	}
})//tested

router.get('/projects/:id/add' ,async(req ,res)=>{//pt filter nu merge cu post .. idk why

	try{
		let filter = req.query.filter?  parseInt(req.query.filter) : 0
		if(filter!=0){
			console.log(filter)
			let project =await Project.findByPk(req.params.id)
			if(project){
				let ids = req.query.ids.split(" ")
				
				console.log(ids)
			

				await ids.map((e)=>{
					let uId = parseInt(e,10)
					console.log(e+"este un element")
					let pId= parseInt(req.params.id)
					 TeamMember.create({projectId:pId,userId:uId})
					console.log("created")
				})
		
			}
			res.status(200).json({message : "made it"})


		}else
		{
			res.status(400).json({message : 'no filters'})
		}
	}
	catch(err){
		console.warn(err)
		res.status(404).json({message : "internal server error"})
	}
})//adauga team member la proiect
router.get('/projects/:id/addT' ,async(req ,res)=>{//pt filter nu merge cu post .. idk why

	try{
		let filter = req.query.filter?  parseInt(req.query.filter) : 0
		if(filter!=0){
			console.log(filter)
			let project =await Project.findByPk(req.params.id)
			if(project){
				let ids = req.query.ids.split(" ")
				
				console.log(ids)
			

				await ids.map((e)=>{
					let uId = parseInt(e,10)
					console.log(e+"este un element")
					let pId= parseInt(req.params.id)
					 TeamTester.create({projectId:pId,userId:uId})
					console.log("created")
				})
		
			}
			res.status(200).json({message : "made it"})


		}else
		{
			res.status(400).json({message : 'no filters'})
		}
	}
	catch(err){
		console.warn(err)
		res.status(404).json({message : "internal server error"})
	}
})//adauga tester
router.get('/projects/:id/bugs', async (req,res)=>{
	try{
		let bug= await Bug.findAll({where : {projectId : req.params.id}})
		res.status(200).json(bug)
	}catch(err){
		console.warn(err)
		res.status(404).json({message : "internal server error"})
	}
})

Project.hasMany(Bug) //project id wil be added on bug table

module.exports = router
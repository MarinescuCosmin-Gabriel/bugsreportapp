'use strict '
const express= require('express')
const router = express.Router()
const Sequelize = require('sequelize')
const cors = require('cors') 
const sequelize = new Sequelize(process.env.DB,process.env.DB_USER,process.env.DB_PASSWORD,{
	dialect : 'mysql',

})
router.use(cors())
const TeamTester = sequelize.import('../models/teamTester-model')
router.delete('/teamTesters/:id',async(req,res)=>{try{
		let teamT = await TeamTester.findByPk(req.params.id)
		if(teamT){
			await teamT.destroy()
			res.status(202).json({message : "accepted"})
		}else{
			res.status(404).json({message : "not found"})
		}

	}catch(err){
		console.warn(err)
		res.status(404).json({message : "internal server error"})
	}
})//tested
router.get('/teamTesters',async (req,res)=>{
	let teamTester =await TeamTester.findAll()
	res.status(200).json(teamTester)
})//tested
router.post('/teamTesters',async(req,res)=>{
		try{
			if(req.query.bulk&&req.query.bulk =='on'){
				await TeamTester.bulkCreate(req.body)
				res.status(201).json({message : "bulk inserted succesfully"})
			}else{
				await TeamTester.create(req.body)
				res.status(201).json({message : " inserted succesfully"})
			}
				
		}catch(err){
			console.warn(err)
			res.status(500).json({message:'internal server error'})
		}

})//tested
router.get('/teamTesters/:id', async (req,res)=>{
	try{
		let teamTester= await TeamTester.findAll({where : {userId : req.params.id}})
		res.status(200).json(teamTester)
	}catch(err){
		console.warn(err)
		res.status(404).json({message : "internal server error"})
	}
})


router.put('/teamTesters/:id',async(req,res)=>{try{
		let teamM = await TeamTester.findByPk(req.params.id)
		if(teamM){
			await teamM.update(req.body)
			res.status(202).json({message : "accepted"})
		}else{
			res.status(404).json({message : "not found"})
		}

	}catch(err){
		console.warn(err)
		res.status(404).json({message : "internal server error"})
	}
})//tested


module.exports = router
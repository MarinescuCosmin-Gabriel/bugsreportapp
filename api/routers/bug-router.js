'use strict '
const express= require('express')
const router = express.Router()
const Sequelize = require('sequelize')
const cors = require('cors') 
const sequelize = new Sequelize(process.env.DB,process.env.DB_USER,process.env.DB_PASSWORD,{
	dialect : 'mysql',

})

const Bug = sequelize.import('../models/bug-model')
router.use(cors())
router.get('/bugs',async (req,res)=>{
	let bug =await Bug.findAll()
	res.status(200).json(bug)
}) 
router.get('/bugs/:id',async (req,res)=>{

	let bug = await Bug.findByPk(req.params.id)  
	if(bug){
		

		res.status(200).json(bug)
	}else{
		res.status(404).json({message: 'not found'})
	}


})

router.post('/bugs/:id/:uid',async(req,res)=>{
		try{
			if(req.query.bulk&&req.query.bulk =='on'){
				await Bug.bulkCreate(req.body)
				res.status(201).json({message : "bulk inserted succesfully"})
			}else{

				await Bug.create({
					description : req.body.description , 
					severity : req.body.severity,
					priority : req.body.priority,
					link : req.body.link,
					projectId: req.params.id,
					reportedBy: req.params.uid
				})
				res.status(201).json({message : " inserted succesfully"})
			}
				
		}catch(err){
			console.warn(err)
			res.status(500).json({message:'internal server error'})
		}

})//tested

router.delete('/bugs/:id',async(req,res)=>{try{
		let user = await Bug.findByPk(req.params.id)
		if(user){
			await user.destroy()
			res.status(202).json({message : "accepted"})
		}else{
			res.status(404).json({message : "not found"})
		}

	}catch(err){
		console.warn(err)
		res.status(404).json({message : "internal server error"})
	}


})//tested


router.put('/bugs/:id',async(req,res)=>{try{
		let user = await Bug.findByPk(req.params.id)
		if(user){
			await user.update(req.body)
			res.status(202).json({message : "accepted"})
		}else{
			res.status(404).json({message : "not found"})
		}

	}catch(err){
		console.warn(err)
		res.status(404).json({message : "internal server error"})
	}


})//tested

module.exports = router
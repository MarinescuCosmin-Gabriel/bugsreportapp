'use strict '
const express= require('express')
const router = express.Router()
const Sequelize = require('sequelize')
const cors = require('cors') 
const Op = Sequelize.Op
const sequelize = new Sequelize(process.env.DB,process.env.DB_USER,process.env.DB_PASSWORD,{
	dialect : 'mysql',

})

const User = sequelize.import('../models/user-model')
const Project = sequelize.import('../models/project-model')
const TeamTester = sequelize.import('../models/teamTester-model')
const TeamMember = sequelize.import('../models/teamMember-model')
const Bug = sequelize.import('../models/bug-model')
router.use(cors())
//import pt vizibilitate router de folosit

router.get('/users/:id/testing', async(req,res) =>{
    let user =await User.findByPk(req.params.id)
    if(user){
        let teams =await TeamTester.findAll({where :{userId : user.id}})
        if(teams){
            let projIds =teams.map((e)=>{
                let i = e.projectId
                return i
			})
			if(projIds.length == 0){
				res.status(500).json({message : "error"})
			}
			console.log(projIds)
        let projects = await Project.findAll({where : {id : {
            [Op.or] : projIds
        }}})
        res.status(200).json(projects)
        }
    }else{
        res.status(500).json({message : "error"})
    }
})

router.get('/users/:id/developer', async(req,res) =>{
    let user =await User.findByPk(req.params.id)
    if(user){
        let teams =await TeamMember.findAll({where :{userId : user.id}})
        if(teams){
            let projIds =teams.map((e)=>{
                let i = e.projectId
                return i
			})
			if(projIds.length == 0){
				res.status(500).json({message : "error"})
			}
            console.log(projIds)
        let projects = await Project.findAll({where : {id : {
            [Op.or] : projIds
        }}})
        res.status(200).json(projects)
        }
    }else{
        res.status(500).json({message : "error"})
    }
})//projcs avalabile for developing


router.post('/login', async(req,res )=>{
	try{
			let user = await User.findOne({where : {
				email : req.body.email
			}})
			if(user){
				if(user.password===req.body.password){
					res.status(200).json(user)
				}else{
					res.status(404).json({message : "password or email incorrect"})
				}

			}
	}catch(err){
		res.status(404).json({message : "not found"})
	}
})//<---- verifica email si parola, get  user data daca exista

router.get('/users',async (req,res)=>{

		let users =await User.findAll();
		res.status(200).json(users)

})//returneaza toti userii


router.post('/users',async(req,res)=>{
		try{
			
				let user = await User.findOne({where : {email : req.body.email}})
				if(user){
							res.status(400).json({message : " email already exists"})
				}else{		
							await User.create(req.body)
						    res.status(201).json({message : " inserted succesfully"})
					 }
				
			
				
		}catch(err){
			console.warn(err)
			res.status(500).json({message:'internal server error'})
		}

})//daca emailul nu a mai fost folosit insereaza date



router.get('/users/:id',async (req,res)=>{

	let user = await User.findByPk(req.params.id) //nu scrie constanta la fel ca tabela de mai sus !! 
	if(user){
		

		res.status(200).json(user)
	}else{
		res.status(404).json({message: 'not found'})
	}


})// get user identified by id


router.delete('/users/:id',async(req,res)=>{try{
		let user = await User.findByPk(req.params.id)
		if(user){
			await user.destroy()
			res.status(202).json({message : "accepted"})
		}else{
			res.status(404).json({message : "not found"})
		}

	}catch(err){
		console.warn(err)
		res.status(404).json({message : "internal server error"})
	}
})//sterge user id


router.put('/users/:id',async(req,res)=>{
	try{
		let user = await User.findByPk(req.params.id)
		if(user){
			await user.update(req.body)
			res.status(202).json({message : "accepted"})
		}else{
			res.status(404).json({message : "not found"})
		}

	}catch(err){
		console.warn(err)
		res.status(404).json({message : "internal server error"})
	}
})//update user data 


router.post('/create',async (req,res)=>{
		try{
				await sequelize.sync({force : true})
				res.status(201).json({message : 'created'})
		}catch(err){
			console.warn(err)
			res.status(500).json({message : 'internal server error'})
		}
})///DEV ONLY : initializare db  ::: :


router.post('/users/:id/projects/:pid', async(req,res)=>{
	try{
		let user = await User.findByPk(req.params.id)
		let project = await Project.findByPk(req.params.pid)
		if(user){
			if(project){
					let pId = project.id
					let uId = user.id

					await TeamTester.create({projectId : pId,userId : uId})
					res.status(200).json({message : "tester succesfully added"})
			}else{
						res.status(400).json({message : "internal server error :"})
			}
		}else{
					res.status(400).json({message : "internal server error"})
		}

	}catch(err){
		console.log(err)
		res.status(400).json({message : "internal server error"})
	}
})
// adauga tester la proiect


// router.get('/users/:id/projects', async(req,res) =>{
// 	let user =await User.findByPk(req.params.id)
// 	if(user){
// 		let teams =await TeamTester.findAll({where :{userId : user.id}})
// 		if(teams){
// 			let projIds =teams.map((e)=>{
// 				let i = e.projectId
// 				return i
// 			})
// 			console.log(projIds)
// 		let projects = await Project.findAll({where : {id : {
// 			[Op.or] : projIds
// 		}}})
// 		res.status(200).json(projects)
// 		}
// 	}else{
// 		res.status(500).json({message : "error"})
// 	}
//})


module.exports = router
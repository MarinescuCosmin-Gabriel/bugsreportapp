'use strict '
const express= require('express')
const router = express.Router()
const Sequelize = require('sequelize')
const cors = require('cors') 
const sequelize = new Sequelize(process.env.DB,process.env.DB_USER,process.env.DB_PASSWORD,{
	dialect : 'mysql',

})

const TeamMember = sequelize.import('../models/teamMember-model')
router.use(cors())

router.get('/teamMembers',async (req,res)=>{
	let teamMember =await TeamMember.findAll()
	res.status(200).json(teamMember)
})//tested

router.get('/teamMembers/:pid',async (req,res)=>{
	try{
		let teamMembers= await TeamMember.findAll({where : {projectId : req.params.pid}})
		res.status(200).json(teamMembers)
	}catch(err){
		console.warn(err)
		res.status(404).json({message : "internal server error"})
	}
	


})

router.post('/teamMembers',async(req,res)=>{
		try{
			if(req.query.bulk&&req.query.bulk =='on'){
				await TeamMember.bulkCreate(req.body)
				res.status(201).json({message : "bulk inserted succesfully"})
			}else{
				await TeamMember.create(req.body)
				res.status(201).json({message : " inserted succesfully"})
			}
				
		}catch(err){
			console.warn(err)
			res.status(500).json({message:'internal server error'})
		}

})//tested

router.delete('/teamMembers/:id',async(req,res)=>{try{
		let teamM = await TeamMember.findByPk(req.params.id)
		if(teamM){
			await teamM.destroy()
			res.status(202).json({message : "accepted"})
		}else{
			res.status(404).json({message : "not found"})
		}

	}catch(err){
		console.warn(err)
		res.status(404).json({message : "internal server error"})
	}
})//tested


router.put('/teamMembers/:id',async(req,res)=>{try{
		let teamM = await TeamMember.findByPk(req.params.id)
		if(teamM){
			await teamM.update(req.body)
			res.status(202).json({message : "accepted"})
		}else{
			res.status(404).json({message : "not found"})
		}

	}catch(err){
		console.warn(err)
		res.status(404).json({message : "internal server error"})
	}
})//tested

module.exports = router
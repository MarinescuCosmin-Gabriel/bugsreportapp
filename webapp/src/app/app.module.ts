import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';



import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WelcomeComponent } from './home/welcome.component';
import { PageNotFoundComponent } from './page-not-found.component';
import {HttpClientJsonpModule} from '@angular/common/http'


import { UserModule } from './user/user.module';
import { ProjectDeveloperComponent } from './projects-developer/project-developer-list';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ProjectTestingComponent } from './projects-testing/project-testing-list';
import { ProjectDetailComponent } from './projects/project-detail.component';
import { ProjectModule } from './projects/project.module';

//import { ProjectDeveloperModule } from './projects-developer/projects-developer-module';
//import { ProjectDeveloperModule } from './projects-developer/projects-developer-module';




@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    HttpClientJsonpModule,
    UserModule,
    AppRoutingModule,
    ProjectModule,
    
    
    
    
  ],
  declarations: [
    AppComponent,
    WelcomeComponent,
    PageNotFoundComponent,
    
    
    
    
    
  
  ],
  
  bootstrap: [AppComponent]
})
export class AppModule { }

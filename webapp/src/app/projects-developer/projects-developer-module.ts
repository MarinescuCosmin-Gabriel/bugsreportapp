import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProjectResolver } from '../projects/project-resolver.service';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';


import { ProjectDeveloperComponent } from './project-developer-list';

import { ProjectModule } from '../projects/project.module';
import { ProjectDetailComponent } from '../projects/project-detail.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ProjectModule,
    
    RouterModule.forChild([
      
     {
        path: ':id',
        component: ProjectDetailComponent,
        resolve: { resolvedData: ProjectResolver },
        
      } ,
     

    
        
    ])
  ],
  declarations: [
        
    
    
    
    
    
   
   
  ]
})
export class ProjectDeveloperModule { }
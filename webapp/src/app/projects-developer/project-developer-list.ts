import { Component, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClientModule} from '@angular/common/http';
import {  Project2 } from '../projects/project';
import { ProjectService } from '../projects/project.service';
import { AuthService } from 'src/app/user/auth.service';
import { User } from 'src/app/user/user2';

@Component({
    templateUrl: './project-developer-list.html',
    styleUrls: ['./project-developer-list.css']
})

export class ProjectDeveloperComponent implements OnInit{
    pagetTitle= 'Developer Projects';
    errorMessage='';
    _listFilter='';
    currentUser: User;

    get listFilter(): string {
        return this._listFilter;
      }
    
      set listFilter(value: string) {
        this._listFilter = value;
        this.filteredProjects = this.listFilter ? this.performFilter(this.listFilter) : this.projects;
      }
      initializare_proiect_click(event: any)
      {
          console.log("proba",event);
      }
      filteredProjects: Project2[] = [];
      projects: Project2[] = [];
     
    
      constructor(private projectService: ProjectService,
                  private route: ActivatedRoute,
                  private authService: AuthService) { }
    
    
               
    
      ngOnInit(): void {
        this.listFilter= this.route.snapshot.queryParamMap.get('filterBy') || '';
        
        //this.currentUser= this.authService.getcurrentUserValue();
    
        this.projectService.getDeveloperProjects().subscribe({
          next: projects => {
            this.projects = projects;
            console.log(projects)
            this.filteredProjects = this.performFilter(this.listFilter);
          },
          error: err => this.errorMessage = err
        });
    
    
    
      
      }
    
      performFilter(filterBy: string): Project2[] {
        filterBy = filterBy.toLocaleLowerCase();
        return this.projects.filter((project: Project2) =>
          project.name.toLocaleLowerCase().indexOf(filterBy) !== -1);
      }
    
    
}

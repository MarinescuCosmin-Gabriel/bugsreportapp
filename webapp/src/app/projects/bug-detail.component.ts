import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import {  ProjectResolved, Project2, Bug, BugResolved,  } from './project';
import { ProjectService } from './project.service';
import { User } from '../user/user2';

import { UserService } from '../user/user.service';
import { Observable } from 'rxjs';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AuthService } from '../user/auth.service';

@Component({
  templateUrl: './bug-detail.component.html',
  styleUrls: ['./bug-detail.component.css']
})
export class BugDetailComponent implements OnInit {
  pageTitle = 'Bug Detail';
  bug: Bug;
  errorMessage: string;
  user_who_reported_bug: User;
  current_user: User;

  constructor(private route: ActivatedRoute,
      private projectService: ProjectService, private authService: AuthService,private userService: UserService) { 
    console.log("s-a apelat constructor");
  }

  ngOnInit(): void {
    console.log("s-a apelat onInit");
    
    const resolvedData: BugResolved =
      this.route.snapshot.data['resolvedData'];
    this.errorMessage = resolvedData.error;
    console.log(this.errorMessage);
    this.onBugRetrieved(resolvedData.bug);
    console.log(this.bug);
    console.log("reportedBY:"+ this.bug.reportedBy);
    this.current_user=this.authService.getcurrentUserValue();
    
   
    
    this.userService.getUser(this.bug.reportedBy).subscribe({
      next:user =>{
        this.user_who_reported_bug=user;
        console.log(user);
      }
    })
    this.authService.teamTester_Url='http://localhost:8080/user-api/users/';
    console.log(this.authService.teamTester_Url);
   


  }
  
  seteazaassignedTo()
  {
    this.bug.assignedTo=this.current_user.email;
    console.log(this.bug);
    this.projectService.updateBug(this.bug).subscribe();
    
  }

  onBugRetrieved(bug: Bug): void {
    this.bug = bug;

    if (this.bug) {
        console.log("project detail cu title: "+ this.bug.description)
      this.pageTitle = `Bug Detail: ${this.bug.description}`;
    } else {
      this.pageTitle = 'No project found';
    }
  }
}

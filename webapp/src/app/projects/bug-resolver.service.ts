import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { ProjectResolved, BugResolved } from './project';
import { ProjectService } from './project.service';

@Injectable({
  providedIn: 'root'
})
export class BugResolver implements Resolve<BugResolved> {

  constructor(private projectService: ProjectService) { }

  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<BugResolved> {
    const idb = route.paramMap.get('idb');
    if (isNaN(+idb)) {
      const message = `Project id was not a number: ${idb}`;
      console.error(message);
      return of({ bug: null, error: message });
    }
    console.log("resolver care ia id fara eroare");
    return this.projectService.getBug(+idb)
      .pipe(
        map(bug => ({ bug: bug })),
        catchError(error => {
          const message = `Retrieval error: ${error}`;
          console.error(message);
          return of({ bug: null, error: message });
        })
      );


      
  }

}

import { Component, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import {  Project2 } from '../project';
import { ProjectService } from '../project.service';
import { AuthService } from 'src/app/user/auth.service';
import { User } from 'src/app/user/user2';

@Component({
    templateUrl: './project-testing-list.html',
    styleUrls: ['./project-testing-list.css']
})

export class ProjectTestingComponent implements OnInit{
    pagetTitle= 'Testing Projects';
    errorMessage='';
    _listFilter='';
    currentUser: User;

    get listFilter(): string {
        return this._listFilter;
      }
    
      set listFilter(value: string) {
        this._listFilter = value;
        this.filteredProjects = this.listFilter ? this.performFilter(this.listFilter) : this.projects;
      }
    
      filteredProjects: Project2[] = [];
      projects: Project2[] = [];
     
    
      constructor(private projectService: ProjectService,
                  private route: ActivatedRoute,
                  private authService: AuthService) { }
    
    
               
    
      ngOnInit(): void {
        this.listFilter= this.route.snapshot.queryParamMap.get('filterBy') || '';
        this.currentUser= this.authService.getcurrentUserValue();
    
    
        this.projectService.getTestingProjects().subscribe({
          next: projects => {
            this.projects = projects;
            console.log(projects)
            this.filteredProjects = this.performFilter(this.listFilter);
          },
          error: err => this.errorMessage = err
        });
    
    
    
      
      }
    
      performFilter(filterBy: string): Project2[] {
        filterBy = filterBy.toLocaleLowerCase();
        return this.projects.filter((project: Project2) =>
          project.name.toLocaleLowerCase().indexOf(filterBy) !== -1);
      }
    
    
}

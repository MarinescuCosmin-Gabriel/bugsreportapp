import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of, throwError } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';

import { Project, Project2, Bug, TeamTester } from './project';
import { AuthService } from '../user/auth.service';
import { LoginComponent } from '../user/login.component';
import { ProjectDetailComponent } from './project-detail.component';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  private projectsUrl = 'http://localhost:8080/project-api/projects';
  concatenare = true;
  concatenare_teamtester=true;
  projects_bugs_Url='http://localhost:8080/project-api/projects';
  project_bug_Url='http://localhost:8080/bug-api/bugs';
  
  public curenntSavingProject: Project2;
  public curenntSavingProjectFromDB: Project2;
  //project_bug_update_url=
  concat_bug=true;
  constructor(private http: HttpClient , private authService: AuthService, ) { }
  
  getProjects(): Observable<Project2[]> {
    
    return this.http.get<Project2[]>(this.projectsUrl)
      .pipe(
        tap(data => console.log(JSON.stringify(data))),
        catchError(this.handleError)
      );
  }
  updateBug(bug: Bug): Observable<Bug> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const aux = 'http://localhost:8080/bug-api/bugs';
    const url = `${aux}/${bug.id}`;
    console.log(url);
    return this.http.put<Bug>(url, bug, { headers })
      .pipe(
        tap(() => console.log('updateBug: ' + bug.id)),
        map(() => bug),
        catchError(this.handleError)
      );
  }
  getBugs(id : number): Observable<Bug[]>{
    this.projects_bugs_Url='http://localhost:8080/project-api/projects';
      this.projects_bugs_Url=this.projects_bugs_Url+'/'+id+'/bugs';
      this.concatenare=false;
    
    console.log(this.projects_bugs_Url);
    return this.http.get<Bug[]>(this.projects_bugs_Url)
      .pipe(
        tap(data => console.log(JSON.stringify(data))),
        catchError(this.handleError)
      );
  }
  getBug(id: number): Observable<Bug>{
    
    this.project_bug_Url='http://localhost:8080/bug-api/bugs';
      this.project_bug_Url = this.project_bug_Url+'/'+id;
      
    
    return this.http.get<Bug>(this.project_bug_Url)
      .pipe(
        tap(data => console.log('getBug ' + JSON.stringify(data))),
        catchError(this.handleError)
      );
  }
  createTeamTester(teamTester: TeamTester): Observable<TeamTester> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    console.log(teamTester);
    
    this.authService.teamTester_Url=this.authService.teamTester_Url+teamTester.userId+"/projects/"+teamTester.projectId;
    
    console.log(this.authService.teamTester_Url);

    return this.http.post<TeamTester>(this.authService.teamTester_Url, teamTester, { headers })
      .pipe(
        tap(data => console.log('createTester: ' + JSON.stringify(data))),
        catchError(this.handleError)
      );
  }


  addTeamMembersToProject(membersId: string): Observable<string> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const projectId = this.curenntSavingProject.id;
    const urlFinal = "add?filter=2&ids=" + membersId;
    const url = `${this.projectsUrl}/${projectId}/${urlFinal}`;
    this.curenntSavingProject = null;
    console.log(url)
    return this.http.get<string>(url)
      .pipe(
        tap(data => console.log('getProject: ' + JSON.stringify(data))),
        catchError(this.handleError)
      );
  }
  getDeveloperProjects(): Observable<Project2[]> {
    
      
    return this.http.get<Project2[]>(this.authService.projects_developers_Url)
      .pipe(
        tap(data => console.log(JSON.stringify(data))),
        catchError(this.handleError)
      );
  }
  getTestingProjects(): Observable<Project2[]> {
    
    return this.http.get<Project2[]>(this.authService.projects_testing_Url)
      .pipe(
        tap(data => console.log(JSON.stringify(data))),
        catchError(this.handleError)
      );
  }
  getProject(id: number): Observable<Project2> {
    if (id === 0) {
      return of(this.initializeProject());
    }
    this.projects_bugs_Url='http://localhost:8080/project-api/projects';
    this.concatenare=true;
    const url = `${this.projectsUrl}/${id}`;
    
    return this.http.get<Project2>(url)
      .pipe(
        tap(data => console.log('getProject: ' + JSON.stringify(data))),
        catchError(this.handleError)
      );
  }

  createProject(Project: Project2): Observable<Project2> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post<Project2>(this.projectsUrl, Project, { headers })
      .pipe(
        tap(data => this.curenntSavingProject = data),
        catchError(this.handleError)
      );
  }

  deleteProject(id: number): Observable<{}> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const url = `${this.projectsUrl}/${id}`;
    return this.http.delete<Project2>(url, { headers })
      .pipe(
        tap(data => console.log('deleteProject: ' + id)),
        catchError(this.handleError)
      );
  }

  updateProject(Project: Project2): Observable<Project2> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const url = `${this.projectsUrl}/${Project.id}`;
    return this.http.put<Project2>(url, Project, { headers })
      .pipe(
        tap(() => console.log('updateProject: ' + Project.id)),
        map(() => Project),
        catchError(this.handleError)
      );
  }

  private handleError(err) {

    let errorMessage: string;
    if (err.error instanceof ErrorEvent) {

      errorMessage = `An error occurred: ${err.error.message}`;
    } else {
      errorMessage = `Backend returned code ${err.status}: ${err.body.error}`;
    }
    console.error(err);
    return throwError(errorMessage);
  }

  private initializeProject(): Project2 {
    return {
      id: 0,
      name: null,
      description: null,
      repo: null,
    };
  }
}

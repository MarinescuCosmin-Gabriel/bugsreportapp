import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Project, Project2 } from '../project';
import { UserService } from 'src/app/user/user.service';
import { ProjectService } from '../project.service';

@Component({
  templateUrl: './project-edit-info.component.html'
})
export class ProjectEditInfoComponent implements OnInit {
  @ViewChild(NgForm, { static: false }) projectForm: NgForm;

  errorMessage: string;
  project: Project2;

  constructor(private route: ActivatedRoute,
    private projectService: ProjectService, 
    private router: Router) { }

  ngOnInit(): void {
    this.route.parent.data.subscribe(data => {
      if (this.projectForm) {
        this.projectForm.reset();
      }

      this.project = data['resolvedData'].project;
    });
  }


  isValid(): boolean {
    if (this.project.name &&
      this.project.name.length >= 3 &&
      this.project.repo && this.project.description) {
      return true;
    } else {
      return false;
    }
  }

  saveProject(): void {
    if (this.isValid()) {
      
        this.projectService.createProject(this.project).subscribe({
          next: () => this.onSaveComplete(),
          error: err => this.errorMessage = err
        });
      
    } else {
      this.errorMessage = 'Please correct the validation errors.';
    }
  }

  onSaveComplete(): void {

    
    this.router.navigate(['projects', '0', 'edit', 'tags']);
  }

}

import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { Project, Project2 } from '../project';
import { User } from 'src/app/user/user2';
import { UserService } from 'src/app/user/user.service';

import { FormGroup, FormControl, Validator, Validators } from "@angular/forms";
import { AuthService } from 'src/app/user/auth.service';
import { ProjectService } from '../project.service';
import { error } from 'util';


@Component({
  templateUrl: './project-edit-tags.component.html',
  styleUrls: ['./project-edit-tags.component.css']
})
export class ProjectEditTagsComponent implements OnInit {

  @ViewChild('multiselect', {static: false}) multiSelect;
  private form: FormGroup
  private loadContent: boolean = false;
  private usersDropDownList: User[];
  private currentUser: User;
  private selectedUsers = [];
  private dropdownSettings: IDropdownSettings;
  private usersId = '';
  private errorMessage: string;
  private currentProject: Project2;




  constructor(private route: ActivatedRoute,
    private authService: AuthService,
    private userService: UserService,
    private projectService: ProjectService,
    private router: Router) { }


  ngOnInit(): void {

    this.userService.getUsers().subscribe((users) => {
      this.usersDropDownList = users;
    })

    this.currentUser = this.authService.user;

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'email',
      itemsShowLimit: 10,
      allowSearchFilter: true
    };

    this.setForm();
  }

  setForm() {
    this.form = new FormGroup({
      users: new FormControl(this.usersDropDownList, Validators.required)
    });
    this.loadContent = true;
  }


  

  isValid(): boolean {
    if (this.form.valid) {
      return true;
    } else {
      return false;
    }
  }
  
  get f() { return this.form.controls; }


  public save() {
    if(this.form.valid){
    this.selectedUsers = this.form.value.users;
    this.usersId += this.selectedUsers[0].id;
    
    for(let i = 1; i < this.selectedUsers.length; i++){
      this.usersId += `+${this.selectedUsers[i].id}`;
    }

    console.log(this.usersId);
    }
    
    this.projectService.addTeamMembersToProject(this.usersId).subscribe( (data) => {
      console.log(data);
      
    })

    this.usersId = '';
  }

  public resetForm() {
    this.setForm();
    this.multiSelect.toggleSelectAll();
  }

}




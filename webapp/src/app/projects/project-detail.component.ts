import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import {  ProjectResolved, Project2, Bug, TeamTester,  } from './project';
import { ProjectService } from './project.service';
import { User } from '../user/user2';

import { UserService } from '../user/user.service';
import { Observable } from 'rxjs';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AuthService } from '../user/auth.service';

@Component({
  templateUrl: './project-detail.component.html',
  styleUrls: ['./project-detail.component.css']
})
export class ProjectDetailComponent implements OnInit {
  pageTitle = 'Project Detail';
  project: Project2;
  errorMessage: string;
  users_who_reported_bug: User[]=[];
  bugs: Bug[]=[];
  checkBoxState: boolean;
  teamTester: TeamTester;
  current_user: User;
  

  constructor(private route: ActivatedRoute,private authService: AuthService,
      private projectService: ProjectService,private userService: UserService) { 
    console.log("s-a apelat constructor");
  }

  ngOnInit(): void {
    console.log("s-a apelat onInit");
    
    const resolvedData: ProjectResolved =
      this.route.snapshot.data['resolvedData'];
    this.errorMessage = resolvedData.error;
    console.log(this.errorMessage);
    this.onProjectRetrieved(resolvedData.project);
    console.log(this.project);
    
    this.userService.getUsers().subscribe({
      next:users =>{
        this.users_who_reported_bug=users;
        console.log(users);
      }
    })
    
    

    this.projectService.getBugs(this.project.id).subscribe({
      next: bugs => {
        this.bugs = bugs;
        console.log(bugs)
        
        
      },
      error: err => this.errorMessage = err
    });


  }  

  adauga_tester(){
    this.teamTester={id: null,userId: 0, projectId: 0};
    this.current_user=this.authService.getcurrentUserValue();
    this.teamTester.userId=this.current_user.id;
    this.teamTester.projectId=this.project.id;
    console.log(this.teamTester);
    this.projectService.createTeamTester(this.teamTester).subscribe({
      next: teamTester => {
        this.teamTester = teamTester;
        console.log(teamTester);
      },
      error : err=> this.errorMessage=err
    })
    this.authService.teamTester_Url='http://localhost:8080/user-api/users/';
    console.log("s-a executat crearea tester");
  }
  
  onProjectRetrieved(project: Project2): void {
    this.project = project;

    if (this.project) {
        console.log("project detail cu title: "+ this.project.name)
      this.pageTitle = `Project Detail: ${this.project.name}`;
    } else {
      this.pageTitle = 'No project found';
    }
  }
}

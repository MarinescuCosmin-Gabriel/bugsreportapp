import { User } from '../user/user2';

export interface Project {
  id: number;
  projectName: string;
  description: string;
  projectRepo: string;
}

export interface Project2 {
  id: number;
  name: string;
  description: string;
  repo: string;
}
export interface Bug{
  id: number;
  description: string;
  severity: number;
  priority: number;
  link: string;
  projectId: number;
  assignedTo: string;
  reportedBy: number;
  status: string;

}
export interface BugResolved{
  bug: Bug;
  
  error?: any;
}
export interface TeamTester{
  id: number;
  projectId: number;
  userId: Number;
}
export interface ProjectResolved {
  project: Project2;
  error?: any;
}

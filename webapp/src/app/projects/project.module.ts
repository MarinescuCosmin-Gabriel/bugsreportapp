import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProjectListComponent } from './project-list.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { ProjectResolver } from './project-resolver.service';
import { CommonModule } from '@angular/common';
;


import { ProjectEditComponent } from './projects-edit/project-edit.component';
import { ProjectEditGuard } from './projects-edit/project-edit.guard';
import { ProjectEditInfoComponent } from './projects-edit/project-edit-info.component';
import { ProjectEditTagsComponent } from './projects-edit/project-edit-tags.component';

import { ProjectDetailComponent } from './project-detail.component';
import { ProjectDeveloperComponent } from '../projects-developer/project-developer-list';
import { AppModule } from '../app.module';
import { AuthGuard } from '../user/auth.guard';
import { ProjectTestingComponent } from '../projects-testing/project-testing-list';
import { BugDetailComponent } from './bug-detail.component';
import { BugResolver } from './bug-resolver.service';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgSelectModule,
    NgMultiSelectDropDownModule.forRoot(),
    ReactiveFormsModule,
    
    RouterModule.forChild([
      {
        path: '',
        component: ProjectListComponent
      },
      {
        path: ':id/edit',
        component: ProjectEditComponent,
        canDeactivate: [ProjectEditGuard],
        resolve: { resolvedData: ProjectResolver },
        children: [
          { path: '', redirectTo: 'info', pathMatch: 'full' },
          
          { path: 'info', component: ProjectEditInfoComponent},
          { path: 'tags', component: ProjectEditTagsComponent}
        ]
      },
     
      {
        path: 'developer',component:ProjectDeveloperComponent,canActivate: [AuthGuard]
      },
      {
        path: 'testing',component:ProjectTestingComponent,canActivate:[AuthGuard]
      },
      {
        path: 'developer/:id',component:ProjectDetailComponent,resolve: {resolvedData: ProjectResolver}
      },
      {
        path: 'testing/:id',component:ProjectDetailComponent,resolve: {resolvedData: ProjectResolver}
      },
      {
        path: 'developer/:id/:idb',component:BugDetailComponent,resolve: {resolvedData: BugResolver}
      },
      {
        path: 'testing/:id/:idb',component:BugDetailComponent,resolve: {resolvedData: BugResolver}
      },
      {
        path:':id/:idb',component:BugDetailComponent,resolve: {resolvedData: BugResolver}
      },
      {
        path: ':id',
        component: ProjectDetailComponent,
        resolve: { resolvedData: ProjectResolver },
        
      },
      
    
        
    ]),
    
  ],
  declarations: [
    ProjectListComponent,
    ProjectEditComponent,
    ProjectEditInfoComponent,
    ProjectEditTagsComponent,
    ProjectDetailComponent,
    ProjectDeveloperComponent,
    ProjectTestingComponent,
    BugDetailComponent,
    
   
    
    
    
    
   
    
   
   
  ]
})
export class ProjectModule { }
